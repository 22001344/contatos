//
//  DetalhesViewController2.swift
//  contatos
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

protocol DetalhesViewControllerDelegate {
    func excluirContato(index: Int)
}

class DetalhesViewController2: UIViewController {

    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var endereco: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var numero: UILabel!
    
    
    public var index : Int?
    public var contato: Contato?
    public var delegate : DetalhesViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = contato?.nome
        
        nome.text = contato?.nome
        endereco.text = contato?.endereco
        email.text = contato?.email
        numero.text = contato?.telefone

    }
    

    @IBAction func excluirContato(_ sender: Any) {
        delegate?.excluirContato(index: index!)
        navigationController?.popViewController(animated: true)
    }
    
   

}
